name in ThisBuild := "wasm4s"

scalaVersion in ThisBuild := "2.13.1"

lazy val core = project in file("./core")

lazy val monocleVersion = "2.0.4" // depends on cats 2.x

lazy val builder = (project in file("./builder"))
  .dependsOn(core)
  .settings(
    libraryDependencies ++= Seq(
      "com.github.julien-truffaut" %%  "monocle-core"  % monocleVersion,
      "com.github.julien-truffaut" %%  "monocle-macro" % monocleVersion,
      "org.scalactic"              %% "scalactic"      % "3.1.1",
      "org.scalatest"              %% "scalatest"      % "3.1.1" % "test",
    )
  )
