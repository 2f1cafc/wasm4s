package wasm4s.io

import wasm4s.model._

object TextOutput extends Output[String] {
  def writeValueType(vt: ValueType) = vt match {
    case ValueType.i32 => "i32"
    case ValueType.i64 => "i64"
    case ValueType.f32 => "f32"
    case ValueType.f64 => "f64"
  }

  def writeFuncType(ft: FuncType) = {
    val args = ft.args.map(arg => s"(param ${writeValueType(arg)})")
    val results = ft.results.map(arg => s"(result ${writeValueType(arg)})")
    s"(func ${args.mkString(" ")} ${results.mkString(" ")})"
  }
  def writeModule(mod: WModule): String = {
    val funcTypes = mod.types.map(writeFuncType)
    s"(module \n  (type ${funcTypes.mkString("\n")}))"
  }
}
