package wasm4s.io

import wasm4s.model._

trait Input[T] {
  def readModule(t: T): WModule
}

trait Output[T] {
  def writeModule(module: WModule): T
}

trait IO[T] extends Output[T] with Input[T]
