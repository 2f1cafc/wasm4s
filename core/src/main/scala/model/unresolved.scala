package wasm4s.model.unresolved

import wasm4s.model

/**
  * this package provides support for defining an interim,
  * "unresolved" version of the webassembly program. For example, in
  * the real thing, there are no names of functions or variables, they
  * are just indices. This package allows building up of context-free
  * functions etc which will then be packaged into a module where
  * their indices will be assigned and they can be converted into the
  * real model.
  */

/* TODO - not sure yet if I actually want this package or not ... */

/* rather than an index into the func type db, it just contains an the
 * actual function type. */
case class Func(`type`: model.FuncType, func: model.Func)
