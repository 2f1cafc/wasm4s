package wasm4s

package object model {
  type Idx      = Int
  type TypeIdx  = Idx
  type FuncIdx  = Idx
  type TableIdx = Idx
  type MemIdx   = Idx
}
