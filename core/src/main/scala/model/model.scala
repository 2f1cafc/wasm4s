package wasm4s.model

import scala.collection.immutable.ListSet

sealed trait ResultType

case object Void extends ResultType

sealed trait ValueType extends ResultType

object ValueType {
  case object i32 extends ValueType
  case object i64 extends ValueType
  case object f32 extends ValueType
  case object f64 extends ValueType
}

sealed trait Expr

/* https://webassembly.github.io/spec/core/valid/instructions.html#constant-expressions
 *
 * "In a constant expression instr∗ 𝖾𝗇𝖽 all instructions in instr∗
 * must be constant. A constant instruction instr must be:
 *
 *     + either of the form t.𝖼𝗈𝗇𝗌𝗍 c
 *     + or of the form 𝗀𝗅𝗈𝖻𝖺𝗅.𝗀𝖾𝗍 x
 *
 * in which case C.𝗀𝗅𝗈𝖻𝖺𝗅𝗌[x] must be a global type of the form 𝖼𝗈𝗇𝗌𝗍 t.
 */
sealed trait ConstantExpr extends Expr {
  type V
  def value: V
}

object Const {
  case class i32(value: Int) extends ConstantExpr {
    type V = Int
  }
}

sealed trait ImportDesc
sealed trait ExportDesc
sealed trait ImportExportDesc extends ImportDesc with ExportDesc

case class GlobalType(mutable: Boolean, valueType: ValueType) extends ImportExportDesc

case class FuncType(args: Seq[ValueType], results: Seq[ValueType])
case class Limits(max: Long, min: Option[Long] = None)
case class Func(typeIdx: TypeIdx, locals: Seq[ValueType], body: Expr)
case class TableType(limits: Limits, element: FuncType) extends ImportExportDesc
case class MemType(limits: Limits) extends ImportExportDesc
case class Global(globalType: GlobalType, init: Expr)

/*
 * Element Segments
 *
 * The initial contents of a table is uninitialized. The 𝖾𝗅𝖾𝗆
 * component of a module defines a vector of element segments that
 * initialize a subrange of a table, at a given offset, from a static
 * vector of elements.
 */
case class Elem(table: TableIdx, offset: ConstantExpr, init: Seq[FuncIdx])

/*
 * Data Segments
 *
 * The initial contents of a memory are zero-valued bytes. The 𝖽𝖺𝗍𝖺
 * component of a module defines a vector of data segments that
 * initialize a range of memory, at a given offset, with a static
 * vector of bytes.
 */
case class Data(data: MemIdx, offset: ConstantExpr, init: Seq[Byte])

case class Import(module: String, name: String, desc: ImportDesc)
case class Export(module: String, name: String, desc: ExportDesc)

case class WModule(
  types   : ListSet[FuncType] = ListSet.empty,
  funcs   : Seq[Func]         = Nil,
  tables  : Seq[TableType]    = Nil,
  mem     : Seq[MemType]      = Nil,
  globals : Seq[Global]       = Nil,
  start   : Option[FuncIdx]   = None,
  imports : Seq[Import]       = Nil,
  exports : Seq[Export]       = Nil,
)

// module::={𝗍𝗒𝗉𝖾𝗌 vec(functype),𝖿𝗎𝗇𝖼𝗌 vec(func),𝗍𝖺𝖻𝗅𝖾𝗌 vec(table),𝗆𝖾𝗆𝗌 vec(mem),𝗀𝗅𝗈𝖻𝖺𝗅𝗌 vec(global),𝖾𝗅𝖾𝗆 vec(elem),𝖽𝖺𝗍𝖺 vec(data),𝗌𝗍𝖺𝗋𝗍 start?,𝗂𝗆𝗉𝗈𝗋𝗍𝗌 vec(import),𝖾𝗑𝗉𝗈𝗋𝗍𝗌 vec(export)}
