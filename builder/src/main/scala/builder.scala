package wasm4s.builder

import monocle.Lens
import monocle.macros.GenLens

import wasm4s.model._

/* tools for building from the wasm4s model */

trait WBuilder[T] {
  def get: T
}

case class ModuleBuilder(
  module: WModule,
  funcNames: Map[String, Idx]
) extends WBuilder[WModule] {

  import ModuleBuilder.lens

  def get = module

  def addFunc(f: Func): ModuleBuilder =
    // perhaps assert the type index of the function exists here?
    lens.funcs.modify(_ :+ f)(this)

  /* add `ft` as func type if neccessary, and modify f's typeIdx to
   * match it before adding */
  def addFunc(ft: FuncType, f: Func): ModuleBuilder = {
    val (typeIdx, mb) = addFuncTypeIdx(ft)
    val updatedFunc = lens.funcTypeIdx.set(typeIdx)(f)
    mb.addFunc(updatedFunc)
  }

  def addFunc(f: unresolved.Func): ModuleBuilder = addFunc(f.`type`, f.func)

  def addFuncTypeIdx(ft: FuncType): (TypeIdx, ModuleBuilder) = {
    val mb = lens.funcTypes.modify(_ + ft)(this)
    lens.funcTypes.get(mb).iterator.indexOf(ft) -> mb
  }

  def addFuncType(ft: FuncType): ModuleBuilder = addFuncTypeIdx(ft)._2
}

object ModuleBuilder {
  object lens {
    val funcTypeIdx = GenLens[Func](_.typeIdx)

    val module = GenLens[ModuleBuilder](_.module)
    val funcs = module composeLens GenLens[WModule](_.funcs)
    val funcTypes = module composeLens GenLens[WModule](_.types)
  }

  def empty = new ModuleBuilder(WModule(), Map.empty)

}

case class FunctionBuilder(typ: FuncType, func: Func)

// object ModuleBuilder {

// }
