package wasm4s

import builder._
import model._

object Main extends App {

  // val m: WModule = ModuleBuilder
  //   .empty
  //   .get

  val m = ModuleBuilder
    .empty
    .addFunc(FuncType(args = Nil, results = Seq(ValueType.i32)),
      Func(0, Nil, Const.i32(0)))

  println(io.TextOutput.writeModule(m.get))

}
