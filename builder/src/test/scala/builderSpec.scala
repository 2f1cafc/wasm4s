package wasm4s.builder

import wasm4s.model._

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers

import wasm4s.builder.ModuleBuilder.lens

class BuilderSpec extends AnyFunSpec with Matchers {
  describe("a module builder") {
    it("should add a func type") {
      val ft = FuncType(Seq(ValueType.i32), Seq(ValueType.i32))
      lens.funcTypes.get(ModuleBuilder.empty.addFuncType(ft))
        .toSeq shouldBe Seq(ft)
    }
    it("should only add a func type once") {
      val ft = FuncType(Seq(ValueType.i32), Seq(ValueType.i32))
      lens.funcTypes.get(
        ModuleBuilder.empty
          .addFuncType(ft)
          .addFuncType(ft)
      ).toSeq shouldBe Seq(ft)
    }
    it("should re-use same func type") {
      val ft = FuncType(Seq(ValueType.i32), Seq(ValueType.i32))
      val mb = ModuleBuilder.empty
        .addFuncType(FuncType(Nil, Nil))
        .addFuncType(ft)
        .addFunc(ft, Func(0, Nil, Const.i32(0)))
      lens.funcs.get(mb) shouldBe Seq(Func(1, Nil, Const.i32(0)))
    }
  }
}
